import { App } from "./dist/app";


async function main() {
    const app = new App();
    app.registerRouter()
    await app.listen();
}

main();
