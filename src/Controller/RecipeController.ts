import { Router } from "express";
import { RecipeBusiness } from "../Business/RecipeBusiness";

import { Recipe } from "../Entity/Recipe";

export const router = Router();
const recipesBusiness = new RecipeBusiness;

router.get('/recipes', async (req, res) => {

    res.json(recipesBusiness.getAll());
});
router.post('/recipes', async (req, res) => {
    const recipe = new Recipe(req.body.name, req.body.category, req.body.picture, req.body.score)
    recipesBusiness.add(recipe).then(data => res.status(201).json(data));

});
