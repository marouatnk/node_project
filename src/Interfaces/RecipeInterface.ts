export interface RecipeInterface {
    id: number;
    name: string;
    category: string;
    picture: string;
    note: number;

    getId(): number;

    getName(): string;
    setName(name:string): void;

    getCategory(): string;
    setCategory(category:string): void;

    getPicture(): string;
    setPicture(picture:string): void;

    getNote(): number;
    setNote(note:number): void;
}
