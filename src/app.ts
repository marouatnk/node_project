// import express from 'express';

// const app = express();
// const port = 3000;
// app.get('/', (req, res) => {
//   res.send('The hello page!');
// });
// app.listen(port);
import express = require("express");
import { router } from "./Controller/RecipeController";
const bodyParser = require('body-parser');
// Create a new express app instance

export class App {
    
    private app: express.Application;

    constructor(){
        this.app = express();
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
    }

    async listen(){
        await this.app.listen(3000);
        console.log('Server on port',3000);
    }

    registerRouter(){
        //ajouter controller router
        this.app.use(router)
    }
}
