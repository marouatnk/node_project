import { RecipeInterface } from "../Interfaces/RecipeInterface";
export class Recipe implements RecipeInterface {
    id: number;
    name: string;
    category: string;
    picture: string;
    note: number;

    constructor(id, name, category, picture?, note?) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.picture = picture;
        this.note = note;
    }

    public getId(): number {
        return this.id;
    }
    public setId(id:number): void {
        this.id = id;
    }

    public getName(): string {
        return this.name;
    }
    public setName(name:string): void {
        this.name = name;
    }
    public getCategory(): string {
        return this.category;
    }
    public setCategory(category:string): void {
        this.category = category;
    }
    public getPicture(): string {
        return this.picture;
    }
    public setPicture(picture:string): void {
        this.picture = picture;
    }
    public getNote(): number {
        return this.note;
    }
    public setNote(note:number): void {
        this.note = note;
    }
}

