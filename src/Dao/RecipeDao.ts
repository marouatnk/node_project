import { connection } from "../Data/databaseConnection"
import { promisify } from "util";
import { Recipe } from "../Entity/Recipe";

export class DaoRecipe {
    private query;
    constructor() {
        // Create connection to database : 
        this.query = promisify(connection.query).bind(connection);
    }

    async getAll(): Promise<Recipe[]> {
        let result = await this.query("SELECT * FROM recipes");
        return result.map(row => new Recipe(row['name'], row['category'], row['picture'], row['score'], row['id']));

    }
    async getByID(id:number):Promise<Recipe> {
        let result = await this.query("SELECT * FROM recipes WHERE id=?",[id]);
        return result
    }
    async add(recipes: Recipe): Promise<number> {
        let result = await this.query('INSERT INTO recipes (name,category,picture,score) VALUES (?,?,?,?)', [
            recipes.getName(),
            recipes.getCategory(),
            recipes.getPicture(),
            recipes.getNote()
        ]);
        recipes.setId(result.insertId)
        return recipes.getId();
    }
    async update(recipe: Recipe) {
        let result = await this.query('UPDATE recipes SET name=?,category=?,picture=?,score=? WHERE id=?', [recipe.getName(), recipe.getCategory(), recipe.getPicture(), recipe.getNote()])
        return recipe
    }
}
